
## 0.2.0 [06-02-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-symantec_mgmtcenter!1

---

## 0.1.1 [07-12-2021]

- Initial Commit

See commit 332cb3d

---
