/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-symantec_mgmtcenter',
      type: 'SymantecMgmtcenter',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const SymantecMgmtcenter = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Symantec_mgmtcenter Adapter Test', () => {
  describe('SymantecMgmtcenter Class Tests', () => {
    const a = new SymantecMgmtcenter(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    let alertsUuid = 'fakedata';
    const alertsRaiseAlertBodyParam = {
      count: 7
    };
    describe('#raiseAlert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.raiseAlert(null, null, alertsRaiseAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('...', data.response.uuid);
                assert.equal('...', data.response.received);
                assert.equal('...', data.response.owner);
                assert.equal('PENDING', data.response.state);
                assert.equal('SYSTEM', data.response.category);
                assert.equal('INFO', data.response.severity);
                assert.equal('URGENT', data.response.priority);
                assert.equal('PRIVATE', data.response.scope);
                assert.equal('...', data.response.externalId);
                assert.equal('...', data.response.source);
                assert.equal('...', data.response.sourceType);
                assert.equal('...', data.response.message);
                assert.equal('...', data.response.externalUrl);
                assert.equal('...', data.response.payload);
                assert.equal(12345, data.response.count);
                assert.equal('...', data.response.comment);
                assert.equal('...', data.response.acknowledgedBy);
                assert.equal(true, Array.isArray(data.response.notes));
              } else {
                runCommonAsserts(data, error);
              }
              alertsUuid = data.response.uuid;
              saveMockData('Alerts', 'raiseAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsAddNoteToAlertBodyParam = {
      submittedBy: 'string',
      submittedOn: 'string',
      note: 'string'
    };
    describe('#addNoteToAlert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addNoteToAlert(alertsUuid, alertsAddNoteToAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('...', data.response.uuid);
                assert.equal('...', data.response.received);
                assert.equal('...', data.response.owner);
                assert.equal('CLOSED', data.response.state);
                assert.equal('OTHER', data.response.category);
                assert.equal('FATAL', data.response.severity);
                assert.equal('HIGH', data.response.priority);
                assert.equal('PRIVATE', data.response.scope);
                assert.equal('...', data.response.externalId);
                assert.equal('...', data.response.source);
                assert.equal('...', data.response.sourceType);
                assert.equal('...', data.response.message);
                assert.equal('...', data.response.externalUrl);
                assert.equal('...', data.response.payload);
                assert.equal(12345, data.response.count);
                assert.equal('...', data.response.comment);
                assert.equal('...', data.response.acknowledgedBy);
                assert.equal(true, Array.isArray(data.response.notes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'addNoteToAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlerts(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getAlerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsUpdateAlertBodyParam = {
      count: 5
    };
    describe('#updateAlert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlert(alertsUuid, alertsUpdateAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'updateAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAlert(alertsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('...', data.response.uuid);
                assert.equal('...', data.response.received);
                assert.equal('...', data.response.owner);
                assert.equal('RESOLVED', data.response.state);
                assert.equal('POLICY', data.response.category);
                assert.equal('INFO', data.response.severity);
                assert.equal('URGENT', data.response.priority);
                assert.equal('PUBLIC', data.response.scope);
                assert.equal('...', data.response.externalId);
                assert.equal('...', data.response.source);
                assert.equal('...', data.response.sourceType);
                assert.equal('...', data.response.message);
                assert.equal('...', data.response.externalUrl);
                assert.equal('...', data.response.payload);
                assert.equal(12345, data.response.count);
                assert.equal('...', data.response.comment);
                assert.equal('...', data.response.acknowledgedBy);
                assert.equal(true, Array.isArray(data.response.notes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'getAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alertsAckAlertBodyParam = {
      comment: 'string',
      acknowledgedBy: 'string'
    };
    describe('#ackAlert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ackAlert(alertsUuid, alertsAckAlertBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'ackAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unackAlert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.unackAlert(alertsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'unackAlert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authUserName = 'fakedata';
    const authSetUserApiTokenBodyParam = {};
    describe('#setUserApiToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setUserApiToken(authUserName, authSetUserApiTokenBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'setUserApiToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authGroupName = 'fakedata';
    describe('#getGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroups(authGroupName, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'getGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupPermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupPermissions(authGroupName, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'getGroupPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroup(authGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('AllOperators', data.response.groupName);
                assert.equal('All Operators', data.response.description);
                assert.equal(false, data.response.readOnly);
                assert.equal('...', data.response.externalName);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal('bob.smith', data.response.lastChangedBy);
                assert.equal('2017-07-01T12:30:20', data.response.lastChanged);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'getGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGroupWithPermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGroupWithPermissions(authGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('AllOperators', data.response.groupName);
                assert.equal('All Operators', data.response.description);
                assert.equal(false, data.response.readOnly);
                assert.equal('...', data.response.externalName);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal('bob.smith', data.response.lastChangedBy);
                assert.equal('2017-07-01T12:30:20', data.response.lastChanged);
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'getGroupWithPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authRoleName = 'fakedata';
    describe('#getRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoles(authRoleName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'getRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRole(authRoleName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('OperatorRole', data.response.roleName);
                assert.equal('Operators', data.response.description);
                assert.equal(false, data.response.readOnly);
                assert.equal('bob.jackson', data.response.lastChangedBy);
                assert.equal('2017-07-01T12:30:20', data.response.lastChanged);
                assert.equal(true, Array.isArray(data.response.permissions));
                assert.equal(true, Array.isArray(data.response.reportPermissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'getRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsers(null, null, null, null, authUserName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'getUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissions(null, null, null, null, authUserName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'getPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUser(authUserName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('emma.golden', data.response.userName);
                assert.equal(false, data.response.readOnly);
                assert.equal('cn=emma.golden', data.response.externalUserName);
                assert.equal('Emma', data.response.firstName);
                assert.equal('Golden', data.response.lastName);
                assert.equal('Emma Golden - Security Ops Group', data.response.description);
                assert.equal('2020-01-01', data.response.expiresOn);
                assert.equal('2017-01-03T21:03:00', data.response.lastAccess);
                assert.equal(0, data.response.failureCount);
                assert.equal('emma.golden@localhost.com', data.response.emailAddress);
                assert.equal('...', data.response.phoneNumber);
                assert.equal('999.999.9999', data.response.mobileNumber);
                assert.equal('EXTERNAL', data.response.userType);
                assert.equal('emma.golden', data.response.lastChangedBy);
                assert.equal('2017-07-26T10:23:56', data.response.lastChanged);
                assert.equal(true, Array.isArray(data.response.memberOf));
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'getUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserPermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserPermissions(authUserName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('emma.golden', data.response.userName);
                assert.equal(false, data.response.readOnly);
                assert.equal('cn=emma.golden', data.response.externalUserName);
                assert.equal('Emma', data.response.firstName);
                assert.equal('Golden', data.response.lastName);
                assert.equal('Emma Golden - Security Ops Group', data.response.description);
                assert.equal('2020-01-01', data.response.expiresOn);
                assert.equal('2017-01-03T21:03:00', data.response.lastAccess);
                assert.equal(0, data.response.failureCount);
                assert.equal('emma.golden@localhost.com', data.response.emailAddress);
                assert.equal('...', data.response.phoneNumber);
                assert.equal('999.999.9999', data.response.mobileNumber);
                assert.equal('EXTERNAL', data.response.userType);
                assert.equal('emma.golden', data.response.lastChangedBy);
                assert.equal('2017-07-26T10:23:56', data.response.lastChanged);
                assert.equal(true, Array.isArray(data.response.memberOf));
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal(true, Array.isArray(data.response.permissions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'getUserPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let deviceUuid = 'fakedata';
    let deviceName = 'fakedata';
    const deviceAddDeviceBodyParam = {
      port: 8082,
      https: true,
      monitorOnly: true,
      deployImmediately: true
    };
    describe('#addDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addDevice(deviceAddDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq7l/gmO...', data.response.sshPublicHostKey);
                assert.equal('...', data.response.lastChangedBy);
                assert.equal('...', data.response.lastChanged);
                assert.equal('...', data.response.description);
                assert.equal('...', data.response.model);
                assert.equal('...', data.response.type);
                assert.equal('...', data.response.osVersion);
                assert.equal('...', data.response.serialNumber);
                assert.equal('...', data.response.hardwareSerialNumber);
                assert.equal('...', data.response.applianceId);
                assert.equal('...', data.response.platform);
                assert.equal('...', data.response.build);
                assert.equal('...', data.response.host);
                assert.equal('FULLY_MANAGED', data.response.managementStatus);
                assert.equal('DEPLOYED', data.response.deploymentStatus);
              } else {
                runCommonAsserts(data, error);
              }
              deviceUuid = data.response.uuid;
              deviceName = data.response.name;
              saveMockData('Device', 'addDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDevices(null, null, null, deviceName, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertificates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCertificates(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getCertificates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllCertificates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllCertificates(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getAllCertificates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConnection(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHealth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHealth((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceIncludeEmpty = true;
    describe('#getLicense - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLicense(null, null, null, deviceIncludeEmpty, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSoftware - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSoftware(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getSoftware', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceStatusList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceStatusList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceStatusList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceTypes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSmcDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSmcDevice(deviceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('...', data.response.lastChangedBy);
                assert.equal('...', data.response.lastChanged);
                assert.equal('...', data.response.description);
                assert.equal('...', data.response.model);
                assert.equal('...', data.response.type);
                assert.equal('...', data.response.osVersion);
                assert.equal('...', data.response.serialNumber);
                assert.equal('...', data.response.hardwareSerialNumber);
                assert.equal('...', data.response.applianceId);
                assert.equal('...', data.response.platform);
                assert.equal('...', data.response.build);
                assert.equal('...', data.response.host);
                assert.equal('FULLY_MANAGED', data.response.managementStatus);
                assert.equal('DEPLOYED', data.response.deploymentStatus);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getSmcDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAttributesForDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAttributesForDevice(deviceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'listAttributesForDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceUpdateAttributesForDeviceBodyParam = {};
    describe('#updateAttributesForDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAttributesForDevice(deviceName, deviceUuid, deviceUpdateAttributesForDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'updateAttributesForDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCertificates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceCertificates(deviceUuid, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceCertificates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceExecuteCommandBodyParam = {};
    describe('#executeCommand - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.executeCommand(deviceUuid, deviceExecuteCommandBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'executeCommand', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceConnection(deviceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('HTTP_BASIC_AUTH', data.response.type);
                assert.equal('10.0.2.1', data.response.host);
                assert.equal(12345, data.response.port);
                assert.equal('...', data.response.hostKey);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceHealth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceHealth(deviceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal(true, Array.isArray(data.response.health));
                assert.equal('SSLV-BXB27-CORP', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceLicense - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceLicense(deviceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('sslv', data.response.deviceType);
                assert.equal('...', data.response.manufacturer);
                assert.equal('...', data.response.applianceModel);
                assert.equal('...', data.response.serialNumber);
                assert.equal('2017-09-22T00:00:00', data.response.creationDate);
                assert.equal('2017-09-22T00:00:00', data.response.lastModifiedDate);
                assert.equal('...', data.response.trialComponents);
                assert.equal('2017-09-22T00:00:00', data.response.trialPeriodExpirationDate);
                assert.equal(12345, data.response.adnUserLimit);
                assert.equal(12345, data.response.concurrentUsersWithADN);
                assert.equal(12345, data.response.concurrentUsersWithoutADN);
                assert.equal('OK', data.response.licenseStatus);
                assert.equal(true, Array.isArray(data.response.components));
                assert.equal(true, Array.isArray(data.response.attributes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#monitorDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.monitorDevice(deviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'monitorDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceChangePasswordBodyParam = {
      currentPassword: 'string',
      newPassword: 'string',
      passwordIdentifier: {}
    };
    describe('#changePassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changePassword(deviceUuid, deviceChangePasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'changePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceSoftware - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceSoftware(deviceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal(true, Array.isArray(data.response.software));
                assert.equal('SSLV-BXB27-CORP', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceSoftware', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceStatus(deviceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('string', data.response.checkDate);
                assert.equal('string', data.response.startDate);
                assert.equal(true, Array.isArray(data.response.warnings));
                assert.equal(true, Array.isArray(data.response.errors));
                assert.equal('object', typeof data.response.monitorState);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'getDeviceStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unmonitorDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.unmonitorDevice(deviceUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'unmonitorDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let deviceGroupsUuid = 'fakedata';
    let deviceGroupsName = 'fakedata';
    const deviceGroupsCreateGroupBodyParam = {
      name: 'SG-DEVICE-GROUP',
      description: 'My SGs',
      type: 'string',
      useForPolicyAssignment: false,
      searchOrder: 2
    };
    describe('#createGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createGroup(deviceGroupsCreateGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('...', data.response.description);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('...', data.response.type);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal(false, data.response.cluster);
                assert.equal('34C12DA9-4009-48F9-83E4-EF2BB07A2FAA', data.response.parent);
                assert.equal('89FCC64E-1318-44F8-A8C2-23996C4AEF07', data.response.hierarchy);
                assert.equal(true, data.response.useForPolicyAssignment);
                assert.equal(2, data.response.searchOrder);
              } else {
                runCommonAsserts(data, error);
              }
              deviceGroupsUuid = data.response.uuid;
              deviceGroupsName = data.response.name;
              saveMockData('DeviceGroups', 'createGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsCreateChildGroupBodyParam = {
      cluster: false
    };
    describe('#createChildGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createChildGroup(deviceGroupsUuid, deviceGroupsCreateChildGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('...', data.response.description);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('...', data.response.type);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal(false, data.response.cluster);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'createChildGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsDevice = 'fakedata';
    const deviceGroupsAddDeviceToGroupBodyParam = {
      clusterManaged: true
    };
    describe('#addDeviceToGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addDeviceToGroup(deviceGroupsDevice, deviceGroupsUuid, deviceGroupsAddDeviceToGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'addDeviceToGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRootGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRootGroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getRootGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllGroups(null, null, deviceGroupsName, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getAllGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsUpdateGroupBodyParam = {
      name: 'SG-DEVICE-GROUP',
      description: 'My SGs',
      type: 'string',
      useForPolicyAssignment: false,
      searchOrder: 2
    };
    describe('#updateGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGroup(deviceGroupsUuid, deviceGroupsUpdateGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'updateGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroup(deviceGroupsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('...', data.response.description);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('...', data.response.type);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal(false, data.response.cluster);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getDeviceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAttributesForGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAttributesForGroup(deviceGroupsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'listAttributesForGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceGroupsUpdateAttributesForGroupBodyParam = {};
    describe('#updateAttributesForGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAttributesForGroup(deviceGroupsName, deviceGroupsUuid, deviceGroupsUpdateAttributesForGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'updateAttributesForGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupChildren - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupChildren(deviceGroupsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getDeviceGroupChildren', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceGroupDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceGroupDevices(deviceGroupsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'getDeviceGroupDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uploadFile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.uploadFile(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'uploadFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listFiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listFiles(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'listFiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesUpdateFileBodyParam = {
      name: 'SGOS6_7_0_1.bcsi',
      description: 'SGOS 6.7.0.1',
      fileType: {},
      accessControl: {},
      deviceType: 'sg',
      resourceId: 'SGLicense'
    };
    describe('#updateFile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFile('fakedata', filesUpdateFileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'updateFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFile('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('SGOS6_7_0_1.bcsi', data.response.name);
                assert.equal('SGOS 6.7.0.1', data.response.description);
                assert.equal('bcsi', data.response.extension);
                assert.equal('object', typeof data.response.fileType);
                assert.equal('object', typeof data.response.accessControl);
                assert.equal('sg', data.response.deviceType);
                assert.equal('admin', data.response.lastChangedBy);
                assert.equal('2020-05-13T21:10:04', data.response.lastChangedOn);
                assert.equal('SGLicense', data.response.resourceId);
                assert.equal('2020-05-14T21:22:34', data.response.uploadDate);
                assert.equal(68234, data.response.size);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const filesUuid = 'fakedata';
    describe('#getFileContent - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getFileContent(filesUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'getFileContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobsImportJobDefinitionBodyParam = {
      passphrase: 'string',
      jobData: {}
    };
    describe('#importJobDefinition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importJobDefinition(jobsImportJobDefinitionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D82E6B0B-31E5-4D3E-BBB8-6A65F9D0DC7C', data.response.jobDefinitionUuid);
                assert.equal('DeviceBackups', data.response.sourceJobName);
                assert.equal('DeviceBackups (1)', data.response.targetJobName);
                assert.equal(true, Array.isArray(data.response.errorsAndWarnings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'importJobDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobsRunJobBodyParam = {
      devices: [
        'string'
      ]
    };
    describe('#runJob - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runJob('fakedata', jobsRunJobBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('string', data.response.endDate);
                assert.equal('string', data.response.startDate);
                assert.equal('object', typeof data.response.status);
                assert.equal('string', data.response.error);
                assert.equal('object', typeof data.response.result);
                assert.equal('string', data.response.parentUuid);
                assert.equal(true, Array.isArray(data.response.steps));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'runJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobDefinitions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobDefinitions(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'getJobDefinitions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getArtifactData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getArtifactData(null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'getArtifactData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getArtifact - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getArtifact('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('10.4.12.21 - Blue Coat SG-VA Series_config.json', data.response.name);
                assert.equal('json', data.response.extension);
                assert.equal('object', typeof data.response.fileType);
                assert.equal('admin', data.response.lastChangedBy);
                assert.equal('2020-05-13T21:10:04', data.response.lastChangedOn);
                assert.equal(6243785, data.response.size);
                assert.equal('object', typeof data.response.source);
                assert.equal('SAVE_CONFIG', data.response.jobOperationType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'getArtifact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobsUuid = 'fakedata';
    describe('#getFileContent2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getFileContent2(jobsUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'getFileContent2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listJobStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listJobStatus(null, 555, null, null, 555, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(0, data.response.start);
                assert.equal(true, Array.isArray(data.response.results));
                assert.equal(100, data.response.total);
                assert.equal(20, data.response.limit);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'listJobStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobsUpdateJobDefinitionBodyParam = {
      passphrase: 'string',
      jobData: {}
    };
    describe('#updateJobDefinition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateJobDefinition(jobsUuid, jobsUpdateJobDefinitionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'updateJobDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobDefinition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobDefinition(jobsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('...', data.response.description);
                assert.equal('ANY', data.response.targetType);
                assert.equal('...', data.response.owner);
                assert.equal('...', data.response.schedule);
                assert.equal(true, data.response.enabled);
                assert.equal('object', typeof data.response.target);
                assert.equal(true, Array.isArray(data.response.operations));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'getJobDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelJob - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelJob(jobsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'cancelJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableJobDefinition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disableJobDefinition(jobsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'disableJobDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableJobDefinition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enableJobDefinition(jobsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'enableJobDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportJobDefinition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.exportJobDefinition(jobsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-01-23T11:28:43', data.response.dateExported);
                assert.equal('2.3', data.response.version);
                assert.equal(true, Array.isArray(data.response.jobs));
                assert.equal('object', typeof data.response.errorsAndWarnings);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'exportJobDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getJobStatus(jobsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('string', data.response.endDate);
                assert.equal('string', data.response.startDate);
                assert.equal('object', typeof data.response.status);
                assert.equal('string', data.response.error);
                assert.equal('object', typeof data.response.result);
                assert.equal('string', data.response.parentUuid);
                assert.equal(true, Array.isArray(data.response.steps));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'getJobStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let policiesUuid = 'fakedata';
    let policiesName = 'fakedata';
    const policiesCreatePolicyBodyParam = {
      shared: false,
      replaceVariables: true
    };
    describe('#createPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPolicy(policiesCreatePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('MY-POLICY', data.response.name);
                assert.equal('cpl', data.response.contentType);
                assert.equal('My new policy', data.response.description);
                assert.equal('my_policy', data.response.referenceId);
                assert.equal('null', data.response.tenant);
                assert.equal('api_user', data.response.author);
                assert.equal(false, data.response.shared);
                assert.equal(true, data.response.replaceVariables);
              } else {
                runCommonAsserts(data, error);
              }
              policiesUuid = data.response.uuid;
              policiesName = data.response.name;
              saveMockData('Policies', 'createPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesCreatePolicyContentBodyParam = {
      content: {},
      contentType: 'string',
      schemaVersion: 'string',
      changeDescription: 'string'
    };
    describe('#createPolicyContent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPolicyContent(policiesUuid, policiesCreatePolicyContentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.content);
                assert.equal('...', data.response.schemaVersion);
                assert.equal('object', typeof data.response.revisionInfo);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'createPolicyContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesInstallPolicyBodyParam = {};
    describe('#installPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.installPolicy(policiesUuid, policiesInstallPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('string', data.response.endDate);
                assert.equal('string', data.response.startDate);
                assert.equal('object', typeof data.response.status);
                assert.equal('string', data.response.error);
                assert.equal('object', typeof data.response.result);
                assert.equal('string', data.response.parentUuid);
                assert.equal(true, Array.isArray(data.response.steps));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'installPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesAddNewTargetBodyParam = {
      enabled: true
    };
    describe('#addNewTarget - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addNewTarget(policiesUuid, policiesAddNewTargetBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('...', data.response.uuid);
                assert.equal('...', data.response.deploymentData);
                assert.equal('...', data.response.deploymentType);
                assert.equal(true, data.response.enabled);
                assert.equal('object', typeof data.response.device);
                assert.equal('object', typeof data.response.group);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'addNewTarget', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesTarget = 'fakedata';
    const policiesInstallToPolicyTargetBodyParam = {};
    describe('#installToPolicyTarget - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.installToPolicyTarget(policiesTarget, policiesUuid, policiesInstallToPolicyTargetBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('WARNING', data.response.status);
                assert.equal(true, Array.isArray(data.response.commandMessages));
                assert.equal(true, Array.isArray(data.response.encodedWarnings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'installToPolicyTarget', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicies(null, null, null, policiesName, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesUpdatePolicyBodyParam = {
      name: 'string',
      description: 'string',
      referenceId: 'string',
      replaceVariables: false
    };
    describe('#updatePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatePolicy(policiesUuid, policiesUpdatePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'updatePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicy(policiesUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('MY-POLICY', data.response.name);
                assert.equal('cpl', data.response.contentType);
                assert.equal('...', data.response.description);
                assert.equal('my_policy', data.response.referenceId);
                assert.equal('default', data.response.tenant);
                assert.equal('api_user', data.response.author);
                assert.equal(false, data.response.shared);
                assert.equal(true, data.response.replaceVariables);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAttributes(policiesUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'listAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesUpdateAttributesBodyParam = {};
    describe('#updateAttributes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAttributes(policiesName, policiesUuid, policiesUpdateAttributesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'updateAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyContent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyContent(policiesUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.content);
                assert.equal('...', data.response.schemaVersion);
                assert.equal('object', typeof data.response.revisionInfo);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicyContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesVersion = 'fakedata';
    describe('#getPolicyContentByVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyContentByVersion(policiesUuid, policiesVersion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.content);
                assert.equal('...', data.response.schemaVersion);
                assert.equal('object', typeof data.response.revisionInfo);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicyContentByVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyTargets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyTargets(policiesUuid, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicyTargets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeploymentRecord - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeploymentRecord(policiesTarget, policiesUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getDeploymentRecord', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disablePolicyTarget - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disablePolicyTarget(policiesTarget, policiesUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'disablePolicyTarget', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enablePolicyTarget - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enablePolicyTarget(policiesTarget, policiesUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'enablePolicyTarget', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#previewPolicyTargetAsText - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.previewPolicyTargetAsText(policiesTarget, policiesUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'previewPolicyTargetAsText', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPolicyVersions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPolicyVersions(policiesUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'getPolicyVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let scriptsUuid = 'fakedata';
    let scriptsName = 'fakedata';
    const scriptsNewScriptBodyParam = {
      replaceVariables: true
    };
    describe('#newScript - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.newScript(scriptsNewScriptBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('...', data.response.description);
                assert.equal('...', data.response.type);
                assert.equal('...', data.response.author);
                assert.equal('...', data.response.referenceId);
                assert.equal(true, data.response.replaceVariables);
              } else {
                runCommonAsserts(data, error);
              }
              scriptsUuid = data.response.uuid;
              scriptsName = data.response.name;
              saveMockData('Scripts', 'newScript', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const scriptsCreateScriptContentBodyParam = {
      content: {},
      schemaVersion: 'string',
      changeDescription: 'string'
    };
    describe('#createScriptContent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createScriptContent(scriptsUuid, scriptsCreateScriptContentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.content);
                assert.equal('...', data.response.schemaVersion);
                assert.equal('object', typeof data.response.revisionInfo);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scripts', 'createScriptContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const scriptsExecuteScriptBodyParam = {
      deviceUuids: [
        'string'
      ],
      parameters: {}
    };
    describe('#executeScript - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.executeScript(scriptsUuid, scriptsExecuteScriptBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('string', data.response.endDate);
                assert.equal('string', data.response.startDate);
                assert.equal('object', typeof data.response.status);
                assert.equal('string', data.response.error);
                assert.equal('object', typeof data.response.result);
                assert.equal('string', data.response.parentUuid);
                assert.equal(true, Array.isArray(data.response.steps));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scripts', 'executeScript', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const scriptsInstallScriptBodyParam = [
      'string'
    ];
    describe('#installScript - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.installScript(scriptsUuid, scriptsInstallScriptBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('string', data.response.endDate);
                assert.equal('string', data.response.startDate);
                assert.equal('object', typeof data.response.status);
                assert.equal('string', data.response.error);
                assert.equal('object', typeof data.response.result);
                assert.equal('string', data.response.parentUuid);
                assert.equal(true, Array.isArray(data.response.steps));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scripts', 'installScript', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScripts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScripts(null, scriptsName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scripts', 'getScripts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const scriptsUpdateScriptBodyParam = {
      replaceVariables: false
    };
    describe('#updateScript - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateScript(scriptsUuid, scriptsUpdateScriptBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scripts', 'updateScript', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScript - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScript(scriptsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('...', data.response.description);
                assert.equal('...', data.response.type);
                assert.equal('...', data.response.author);
                assert.equal('...', data.response.referenceId);
                assert.equal(true, data.response.replaceVariables);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scripts', 'getScript', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAttributes2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAttributes2(scriptsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scripts', 'listAttributes2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const scriptsUpdateAttributes2BodyParam = {};
    describe('#updateAttributes2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateAttributes2(scriptsName, scriptsUuid, scriptsUpdateAttributes2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scripts', 'updateAttributes2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScriptContent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScriptContent(scriptsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.content);
                assert.equal('...', data.response.schemaVersion);
                assert.equal('object', typeof data.response.revisionInfo);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scripts', 'getScriptContent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const scriptsVersion = 'fakedata';
    describe('#getScriptContentByVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScriptContentByVersion(scriptsUuid, scriptsVersion, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.content);
                assert.equal('...', data.response.schemaVersion);
                assert.equal('object', typeof data.response.revisionInfo);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scripts', 'getScriptContentByVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScriptVersions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScriptVersions(scriptsUuid, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scripts', 'getScriptVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemStoreId = 'fakedata';
    const systemAllowRestart = true;
    describe('#applySetting - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applySetting(systemStoreId, systemAllowRestart, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'applySetting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAuditRecords - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAuditRecords(null, 555, 555, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(0, data.response.start);
                assert.equal(20, data.response.limit);
                assert.equal(true, Array.isArray(data.response.results));
                assert.equal(100, data.response.total);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'listAuditRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemInfo((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSystemInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMetrics((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSensors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSensors((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSensors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSettings((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemSetSettingValuesBodyParam = {};
    describe('#setSettingValues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setSettingValues(systemStoreId, systemSetSettingValuesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'setSettingValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSettingValues - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSettingValues(systemStoreId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSettingValues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSettingValuesSchema - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSettingValuesSchema(systemStoreId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getSettingValuesSchema', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStorageUsage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStorageUsage((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getStorageUsage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVersion((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('...', data.response.version);
                assert.equal('...', data.response.name);
                assert.equal('...', data.response.id);
                assert.equal('...', data.response.buildNumber);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('System', 'getVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let tenantsUuid = 'fakedata';
    const tenantsCreateTenantBodyParam = {
      system: true
    };
    describe('#createTenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTenant(tenantsCreateTenantBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('...', data.response.externalId);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('...', data.response.description);
                assert.equal(true, data.response.system);
              } else {
                runCommonAsserts(data, error);
              }
              tenantsUuid = data.response.uuid;
              saveMockData('Tenants', 'createTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTenants((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenants', 'getTenants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenantsUpdateTenantBodyParam = {
      system: false
    };
    describe('#updateTenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTenant(tenantsUuid, tenantsUpdateTenantBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenants', 'updateTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTenant(tenantsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('D1055376-3448-4A94-82DB-D9BD30BBB420', data.response.uuid);
                assert.equal('...', data.response.externalId);
                assert.equal('SSLV-BXB27-CORP', data.response.name);
                assert.equal('...', data.response.description);
                assert.equal(true, data.response.system);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenants', 'getTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#clearUserApiToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.clearUserApiToken(authUserName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Auth', 'clearUserApiToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.removeDevice(deviceUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Device', 'removeDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGroup(deviceGroupsUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'deleteGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeDeviceFromGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeDeviceFromGroup(deviceGroupsDevice, deviceGroupsUuid, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DeviceGroups', 'removeDeviceFromGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteFile(filesUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Files', 'deleteFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteJobDefinition - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteJobDefinition(jobsUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'deleteJobDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesDeletePolicyBodyParam = {};
    describe('#deletePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePolicy(policiesUuid, policiesDeletePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'deletePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePolicyTarget - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePolicyTarget(policiesTarget, policiesUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'deletePolicyTarget', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteScript - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteScript(scriptsUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-symantec_mgmtcenter-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Scripts', 'deleteScript', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenant - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTenant(tenantsUuid, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenants', 'deleteTenant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
