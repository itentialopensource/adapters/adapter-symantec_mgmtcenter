## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.


### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Symantec Management Center. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Symantec Management Center.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Symantec_mgmtcenter. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getAlerts(order, callback)</td>
    <td style="padding:15px">getAlerts</td>
    <td style="padding:15px">{base_path}/{version}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">raiseAlert(raiseTrap, sendEmail, body, callback)</td>
    <td style="padding:15px">raiseAlert</td>
    <td style="padding:15px">{base_path}/{version}/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlert(uuid, callback)</td>
    <td style="padding:15px">getAlert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlert(uuid, body, callback)</td>
    <td style="padding:15px">updateAlert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ackAlert(uuid, body, callback)</td>
    <td style="padding:15px">ackAlert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/acknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNoteToAlert(uuid, body, callback)</td>
    <td style="padding:15px">addNoteToAlert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/note?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unackAlert(uuid, callback)</td>
    <td style="padding:15px">unackAlert</td>
    <td style="padding:15px">{base_path}/{version}/alerts/{pathv1}/unacknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroups(groupName, roles, users, callback)</td>
    <td style="padding:15px">getGroups</td>
    <td style="padding:15px">{base_path}/{version}/auth/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupPermissions(groupName, roles, users, callback)</td>
    <td style="padding:15px">getGroupPermissions</td>
    <td style="padding:15px">{base_path}/{version}/auth/groups/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroup(groupName, callback)</td>
    <td style="padding:15px">getGroup</td>
    <td style="padding:15px">{base_path}/{version}/auth/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupWithPermissions(groupName, callback)</td>
    <td style="padding:15px">getGroupWithPermissions</td>
    <td style="padding:15px">{base_path}/{version}/auth/groups/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoles(roleName, callback)</td>
    <td style="padding:15px">getRoles</td>
    <td style="padding:15px">{base_path}/{version}/auth/roles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRole(roleName, callback)</td>
    <td style="padding:15px">getRole</td>
    <td style="padding:15px">{base_path}/{version}/auth/roles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(firstName, lastName, memberOf, roles, userName, userType, callback)</td>
    <td style="padding:15px">getUsers</td>
    <td style="padding:15px">{base_path}/{version}/auth/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPermissions(firstName, lastName, memberOf, roles, userName, userType, callback)</td>
    <td style="padding:15px">getPermissions</td>
    <td style="padding:15px">{base_path}/{version}/auth/users/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUser(userName, callback)</td>
    <td style="padding:15px">getUser</td>
    <td style="padding:15px">{base_path}/{version}/auth/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearUserApiToken(userName, expireInDays, callback)</td>
    <td style="padding:15px">clearUserApiToken</td>
    <td style="padding:15px">{base_path}/{version}/auth/users/{pathv1}/api-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setUserApiToken(userName, body, callback)</td>
    <td style="padding:15px">setUserApiToken</td>
    <td style="padding:15px">{base_path}/{version}/auth/users/{pathv1}/api-token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserPermissions(userName, callback)</td>
    <td style="padding:15px">getUserPermissions</td>
    <td style="padding:15px">{base_path}/{version}/auth/users/{pathv1}/permission?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevices(build, description, model, name, osVersion, platform, type, callback)</td>
    <td style="padding:15px">getDevices</td>
    <td style="padding:15px">{base_path}/{version}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDevice(body, callback)</td>
    <td style="padding:15px">addDevice</td>
    <td style="padding:15px">{base_path}/{version}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificates(dateRevoked, excludeUsedIn, expirationDate, fingerprint, issueDate, issuer, keyUsageNames, selfSigned, serialNumber, signatureAlgorithm, subject, callback)</td>
    <td style="padding:15px">getCertificates</td>
    <td style="padding:15px">{base_path}/{version}/devices/certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCertificates(dateRevoked, expirationDate, fingerprint, issueDate, issuer, keyUsageNames, selfSigned, serialNumber, signatureAlgorithm, subject, callback)</td>
    <td style="padding:15px">getAllCertificates</td>
    <td style="padding:15px">{base_path}/{version}/devices/certificates/usage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConnection(connectionType, type, callback)</td>
    <td style="padding:15px">getConnection</td>
    <td style="padding:15px">{base_path}/{version}/devices/connection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHealth(callback)</td>
    <td style="padding:15px">getHealth</td>
    <td style="padding:15px">{base_path}/{version}/devices/health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicense(activationDate, componentName, expirationDate, includeEmpty, type, callback)</td>
    <td style="padding:15px">getLicense</td>
    <td style="padding:15px">{base_path}/{version}/devices/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSoftware(type, callback)</td>
    <td style="padding:15px">getSoftware</td>
    <td style="padding:15px">{base_path}/{version}/devices/software?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceStatusList(callback)</td>
    <td style="padding:15px">getDeviceStatusList</td>
    <td style="padding:15px">{base_path}/{version}/devices/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTypes(callback)</td>
    <td style="padding:15px">getDeviceTypes</td>
    <td style="padding:15px">{base_path}/{version}/devices/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeDevice(uuid, callback)</td>
    <td style="padding:15px">removeDevice</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSmcDevice(uuid, callback)</td>
    <td style="padding:15px">getSmcDevice</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAttributesForDevice(uuid, callback)</td>
    <td style="padding:15px">listAttributesForDevice</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAttributesForDevice(name, uuid, body, callback)</td>
    <td style="padding:15px">updateAttributesForDevice</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCertificates(uuid, dateRevoked, excludeUsedIn, expirationDate, fingerprint, issueDate, issuer, keyUsageNames, selfSigned, serialNumber, signatureAlgorithm, subject, callback)</td>
    <td style="padding:15px">getDeviceCertificates</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">executeCommand(uuid, body, callback)</td>
    <td style="padding:15px">executeCommand</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/command?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceConnection(uuid, callback)</td>
    <td style="padding:15px">getDeviceConnection</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/connection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceHealth(uuid, callback)</td>
    <td style="padding:15px">getDeviceHealth</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceLicense(uuid, callback)</td>
    <td style="padding:15px">getDeviceLicense</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitorDevice(uuid, callback)</td>
    <td style="padding:15px">monitorDevice</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/monitor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changePassword(uuid, body, callback)</td>
    <td style="padding:15px">changePassword</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceSoftware(uuid, callback)</td>
    <td style="padding:15px">getDeviceSoftware</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/software?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceStatus(uuid, callback)</td>
    <td style="padding:15px">getDeviceStatus</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unmonitorDevice(uuid, callback)</td>
    <td style="padding:15px">unmonitorDevice</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/unmonitor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRootGroups(callback)</td>
    <td style="padding:15px">getRootGroups</td>
    <td style="padding:15px">{base_path}/{version}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createGroup(body, callback)</td>
    <td style="padding:15px">createGroup</td>
    <td style="padding:15px">{base_path}/{version}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllGroups(cluster, description, name, parentUuid, useForPolicyAssignment, callback)</td>
    <td style="padding:15px">getAllGroups</td>
    <td style="padding:15px">{base_path}/{version}/groups/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGroup(uuid, callback)</td>
    <td style="padding:15px">deleteGroup</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroup(uuid, callback)</td>
    <td style="padding:15px">getDeviceGroup</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createChildGroup(uuid, body, callback)</td>
    <td style="padding:15px">createChildGroup</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGroup(uuid, body, callback)</td>
    <td style="padding:15px">updateGroup</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAttributesForGroup(uuid, callback)</td>
    <td style="padding:15px">listAttributesForGroup</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAttributesForGroup(name, uuid, body, callback)</td>
    <td style="padding:15px">updateAttributesForGroup</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupChildren(uuid, callback)</td>
    <td style="padding:15px">getDeviceGroupChildren</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/children?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupDevices(uuid, callback)</td>
    <td style="padding:15px">getDeviceGroupDevices</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeDeviceFromGroup(device, uuid, force, callback)</td>
    <td style="padding:15px">removeDeviceFromGroup</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDeviceToGroup(device, uuid, body, callback)</td>
    <td style="padding:15px">addDeviceToGroup</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listFiles(accessControl, description, deviceType, extension, fileType, lastChangedBy, lastChangedOn, name, uploadDate, callback)</td>
    <td style="padding:15px">listFiles</td>
    <td style="padding:15px">{base_path}/{version}/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadFile(resolution = 'KEEP', bodyFormData, callback)</td>
    <td style="padding:15px">uploadFile</td>
    <td style="padding:15px">{base_path}/{version}/files?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFile(uuid, callback)</td>
    <td style="padding:15px">deleteFile</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFile(uuid, callback)</td>
    <td style="padding:15px">getFile</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFile(uuid, body, callback)</td>
    <td style="padding:15px">updateFile</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileContent(uuid, callback)</td>
    <td style="padding:15px">getFileContent</td>
    <td style="padding:15px">{base_path}/{version}/files/{pathv1}/content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobDefinitions(description, enabled, name, owner, targetType, callback)</td>
    <td style="padding:15px">getJobDefinitions</td>
    <td style="padding:15px">{base_path}/{version}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importJobDefinition(body, callback)</td>
    <td style="padding:15px">importJobDefinition</td>
    <td style="padding:15px">{base_path}/{version}/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArtifactData(extension, fileType, lastChangedBy, lastChangedOn, name, source, callback)</td>
    <td style="padding:15px">getArtifactData</td>
    <td style="padding:15px">{base_path}/{version}/jobs/artifact?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getArtifact(uuid, callback)</td>
    <td style="padding:15px">getArtifact</td>
    <td style="padding:15px">{base_path}/{version}/jobs/artifact/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFileContent2(uuid, callback)</td>
    <td style="padding:15px">getFileContent2</td>
    <td style="padding:15px">{base_path}/{version}/jobs/artifact/{pathv1}/content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listJobStatus(endDate, limit, name, result, start, startDate, status, callback)</td>
    <td style="padding:15px">listJobStatus</td>
    <td style="padding:15px">{base_path}/{version}/jobs/result?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteJobDefinition(uuid, callback)</td>
    <td style="padding:15px">deleteJobDefinition</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobDefinition(uuid, callback)</td>
    <td style="padding:15px">getJobDefinition</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateJobDefinition(uuid, body, callback)</td>
    <td style="padding:15px">updateJobDefinition</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelJob(uuid, callback)</td>
    <td style="padding:15px">cancelJob</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableJobDefinition(uuid, callback)</td>
    <td style="padding:15px">disableJobDefinition</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableJobDefinition(uuid, callback)</td>
    <td style="padding:15px">enableJobDefinition</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportJobDefinition(uuid, callback)</td>
    <td style="padding:15px">exportJobDefinition</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobStatus(uuid, callback)</td>
    <td style="padding:15px">getJobStatus</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}/result?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runJob(uuid, body, callback)</td>
    <td style="padding:15px">runJob</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicies(author, contentType, description, name, referenceId, shared, tenant, callback)</td>
    <td style="padding:15px">getPolicies</td>
    <td style="padding:15px">{base_path}/{version}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicy(body, callback)</td>
    <td style="padding:15px">createPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicy(uuid, body, callback)</td>
    <td style="padding:15px">deletePolicy</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicy(uuid, callback)</td>
    <td style="padding:15px">getPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatePolicy(uuid, body, callback)</td>
    <td style="padding:15px">updatePolicy</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAttributes(uuid, callback)</td>
    <td style="padding:15px">listAttributes</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAttributes(name, uuid, body, callback)</td>
    <td style="padding:15px">updateAttributes</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyContent(uuid, callback)</td>
    <td style="padding:15px">getPolicyContent</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPolicyContent(uuid, body, callback)</td>
    <td style="padding:15px">createPolicyContent</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyContentByVersion(uuid, version, callback)</td>
    <td style="padding:15px">getPolicyContentByVersion</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/content/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">installPolicy(uuid, body, callback)</td>
    <td style="padding:15px">installPolicy</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/install?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyTargets(uuid, targetName, callback)</td>
    <td style="padding:15px">getPolicyTargets</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/targets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNewTarget(uuid, body, callback)</td>
    <td style="padding:15px">addNewTarget</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/targets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePolicyTarget(target, uuid, callback)</td>
    <td style="padding:15px">deletePolicyTarget</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/targets/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeploymentRecord(target, uuid, callback)</td>
    <td style="padding:15px">getDeploymentRecord</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/targets/{pathv2}/deployment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disablePolicyTarget(target, uuid, callback)</td>
    <td style="padding:15px">disablePolicyTarget</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/targets/{pathv2}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enablePolicyTarget(target, uuid, callback)</td>
    <td style="padding:15px">enablePolicyTarget</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/targets/{pathv2}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">installToPolicyTarget(target, uuid, body, callback)</td>
    <td style="padding:15px">installToPolicyTarget</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/targets/{pathv2}/install?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">previewPolicyTargetAsText(target, uuid, callback)</td>
    <td style="padding:15px">previewPolicyTargetAsText</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/targets/{pathv2}/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPolicyVersions(uuid, callback)</td>
    <td style="padding:15px">getPolicyVersions</td>
    <td style="padding:15px">{base_path}/{version}/policies/{pathv1}/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScripts(description, name, type, callback)</td>
    <td style="padding:15px">getScripts</td>
    <td style="padding:15px">{base_path}/{version}/scripts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">newScript(body, callback)</td>
    <td style="padding:15px">newScript</td>
    <td style="padding:15px">{base_path}/{version}/scripts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteScript(uuid, callback)</td>
    <td style="padding:15px">deleteScript</td>
    <td style="padding:15px">{base_path}/{version}/scripts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScript(uuid, callback)</td>
    <td style="padding:15px">getScript</td>
    <td style="padding:15px">{base_path}/{version}/scripts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateScript(uuid, body, callback)</td>
    <td style="padding:15px">updateScript</td>
    <td style="padding:15px">{base_path}/{version}/scripts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAttributes2(uuid, callback)</td>
    <td style="padding:15px">listAttributes2</td>
    <td style="padding:15px">{base_path}/{version}/scripts/{pathv1}/attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAttributes2(name, uuid, body, callback)</td>
    <td style="padding:15px">updateAttributes2</td>
    <td style="padding:15px">{base_path}/{version}/scripts/{pathv1}/attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScriptContent(uuid, callback)</td>
    <td style="padding:15px">getScriptContent</td>
    <td style="padding:15px">{base_path}/{version}/scripts/{pathv1}/content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createScriptContent(uuid, body, callback)</td>
    <td style="padding:15px">createScriptContent</td>
    <td style="padding:15px">{base_path}/{version}/scripts/{pathv1}/content?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScriptContentByVersion(uuid, version, callback)</td>
    <td style="padding:15px">getScriptContentByVersion</td>
    <td style="padding:15px">{base_path}/{version}/scripts/{pathv1}/content/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">executeScript(uuid, body, callback)</td>
    <td style="padding:15px">executeScript</td>
    <td style="padding:15px">{base_path}/{version}/scripts/{pathv1}/execute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">installScript(uuid, body, callback)</td>
    <td style="padding:15px">installScript</td>
    <td style="padding:15px">{base_path}/{version}/scripts/{pathv1}/install?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScriptVersions(uuid, max, start, callback)</td>
    <td style="padding:15px">getScriptVersions</td>
    <td style="padding:15px">{base_path}/{version}/scripts/{pathv1}/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAuditRecords(fromDate, limit, start, toDate, callback)</td>
    <td style="padding:15px">listAuditRecords</td>
    <td style="padding:15px">{base_path}/{version}/system/audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemInfo(callback)</td>
    <td style="padding:15px">getSystemInfo</td>
    <td style="padding:15px">{base_path}/{version}/system/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetrics(callback)</td>
    <td style="padding:15px">getMetrics</td>
    <td style="padding:15px">{base_path}/{version}/system/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSensors(callback)</td>
    <td style="padding:15px">getSensors</td>
    <td style="padding:15px">{base_path}/{version}/system/sensors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSettings(callback)</td>
    <td style="padding:15px">getSettings</td>
    <td style="padding:15px">{base_path}/{version}/system/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSettingValues(storeId, callback)</td>
    <td style="padding:15px">getSettingValues</td>
    <td style="padding:15px">{base_path}/{version}/system/settings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSettingValues(storeId, body, callback)</td>
    <td style="padding:15px">setSettingValues</td>
    <td style="padding:15px">{base_path}/{version}/system/settings/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applySetting(storeId, allowRestart, callback)</td>
    <td style="padding:15px">applySetting</td>
    <td style="padding:15px">{base_path}/{version}/system/settings/{pathv1}/apply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSettingValuesSchema(storeId, callback)</td>
    <td style="padding:15px">getSettingValuesSchema</td>
    <td style="padding:15px">{base_path}/{version}/system/settings/{pathv1}/schema?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStorageUsage(callback)</td>
    <td style="padding:15px">getStorageUsage</td>
    <td style="padding:15px">{base_path}/{version}/system/storage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVersion(callback)</td>
    <td style="padding:15px">getVersion</td>
    <td style="padding:15px">{base_path}/{version}/system/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenants(callback)</td>
    <td style="padding:15px">getTenants</td>
    <td style="padding:15px">{base_path}/{version}/tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTenant(body, callback)</td>
    <td style="padding:15px">createTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTenant(uuid, callback)</td>
    <td style="padding:15px">deleteTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenant(uuid, callback)</td>
    <td style="padding:15px">getTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTenant(uuid, body, callback)</td>
    <td style="padding:15px">updateTenant</td>
    <td style="padding:15px">{base_path}/{version}/tenants/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
